const Responses = require('./API_responses');
exports.handler = async event => {
    console.log('event', event)

    if(!event.pathParameters || !event.pathParameters.ID){
        //failed without an ID
        return Responses._400({message: 'Failed the request withot an ID'});
    }

    let ID = event.pathParameters.ID;

    if(data[ID]){
        //return the data
        return Responses._200(data[ID]);
    }else{
        //return ID not found
        return Responses._400({message: 'ID is not matched with any record'});
    }
}

const data = {
    1234: { name: 'Anna Jones', age: 25, job: 'journalist' },
    7893: { name: 'Chris Smith', age: 52, job: 'teacher' },
    5132: { name: 'Tom Hague', age: 23, job: 'plasterer' },
};